const { Client } = require('pg')

const table = 'users'
const connectionString = 'postgresql://postgres@172.17.0.3:5432/pdb'

function SqlMongo(connectionString, table) {
  this.table = table
  this.client = new Client({ connectionString: connectionString })
  this.values = []
  this.query = ''
  this.client.connect()
}

SqlMongo.prototype.addRecord = function(record, callback) {
  const columns = Object.keys(record).join(',')
  const values = Object.values(record)
  const values1 = Object.keys(record)
    .map((val, ind) => '$' + (ind + 1))
    .join(',')
  this.client.query(
    `INSERT INTO ${this.table}(${columns}) VALUES(${values1}) RETURNING * `,
    values,
    callback,
  )
}

SqlMongo.prototype.addRecords = function(records, callback) {
  records.map(record => this.addRecord(record, callback))
}

SqlMongo.prototype.getAllRecords = function(callback) {
  this.client.query(`SELECT * FROM ${this.table}`, callback)
}

SqlMongo.prototype.getRecords = function(callback) {
  const query = this.query
  const values = this.values
  this.query = ''
  this.values = []
  this.client.query(`${query}`, values, callback)
}

SqlMongo.prototype.find = function(cond) {
  const conditions = Object.keys(cond)
    .map((val, ind) => val + ' = ' + '$' + (ind + 1))
    .join(' AND ')
  this.values = Object.values(cond)
  this.query = `SELECT * FROM users WHERE ${conditions}`
  return this
}

SqlMongo.prototype.limit = function(n) {
  this.query = `${this.query} LIMIT ${n}`
  return this
}

SqlMongo.prototype.where = function(column) {
  const where = /where/i
  if (where.test(this.query)) {
    this.query = `${this.query} AND ${column}`
  } else {
    this.query = `SELECT * FROM ${this.table} WHERE ${column}`
  }
  return this
}

SqlMongo.prototype.equals = function(value) {
  const digitEnd = /.*\d$/i
  if (digitEnd.test(this.query)) {
    this.values.push(value)
    this.query = `${this.query} AND ${
      this.query.match(/.*\s(\w+)\s.+\s.\d$/i)[1]
    } = $${this.values.length}`
  } else {
    this.values.push(value)
    this.query = `${this.query} = $${this.values.length}`
  }
  return this
}

SqlMongo.prototype.gt = function(value) {
  const digitEnd = /.*\d$/i
  if (digitEnd.test(this.query)) {
    this.values.push(value)
    this.query = `${this.query} AND ${
      this.query.match(/.*\s(\w+)\s.+\s.\d$/i)[1]
    } > $${this.values.length}`
  } else {
    this.values.push(value)
    this.query = `${this.query} > $${this.values.length}`
  }
  return this
}

SqlMongo.prototype.lt = function(value) {
  const digitEnd = /.*\d$/i
  if (digitEnd.test(this.query)) {
    this.values.push(value)
    this.query = `${this.query} AND ${
      this.query.match(/.*\s(\w+)\s.+\s.\d$/i)[1]
    } < $${this.values.length}`
  } else {
    this.values.push(value)
    this.query = `${this.query} < $${this.values.length}`
  }
  return this
}

SqlMongo.prototype.gte = function(value) {
  const digitEnd = /.*\d$/i
  if (digitEnd.test(this.query)) {
    this.values.push(value)
    this.query = `${this.query} AND ${
      this.query.match(/.*\s(\w+)\s.+\s.\d$/i)[1]
    } > $${this.values.length}`
  } else {
    this.values.push(value)
    this.query = `${this.query} >= $${this.values.length}`
  }
  return this
}

SqlMongo.prototype.lte = function(value) {
  const digitEnd = /.*\d$/i
  if (digitEnd.test(this.query)) {
    this.values.push(value)
    this.query = `${this.query} AND ${
      this.query.match(/.*\s(\w+)\s.+\s.\d$/i)[1]
    } <= $${this.values.length}`
  } else {
    this.values.push(value)
    this.query = `${this.query} <= $${this.values.length}`
  }
  return this
}

SqlMongo.prototype.sort = function(column, order) {
  if (!order || order === 1) {
    this.query = `${this.query} ORDER BY ${column}`
  } else {
    this.query = `${this.query} ORDER BY ${column} DESC`
  }
  return this
}

const mysql = new SqlMongo(connectionString, table)

mysql.addRecord({ name: 'chandu', age: 26 }, (err, res) => {
  if (err) {
    console.log(err.stack)
  } else {
    console.log(res.rows[0])
  }
})

mysql.addRecords(
  [{ name: 'chandu1', age: 25 }, { name: 'chandu2', age: 30 }],
  (err, res) => {
    if (err) {
      console.log(err.stack)
    } else {
      console.log(res.rows[0])
    }
  },
)

mysql.getAllRecords((err, res) => {
  if (err) {
    console.log(err.stack)
  } else {
    console.log(res.rows)
  }
})

mysql
  .find({ name: 'chandu' })
  .limit(10)
  .getRecords((err, res) => {
    if (err) {
      console.log(err.stack)
    } else {
      console.log(res.rows)
    }
  })

mysql
  .where('id')
  .equals(1)
  .limit(3)
  .getRecords((err, res) => {
    if (err) {
      console.log(err.stack)
    } else {
      console.log('------equals----------', res.rows)
    }
  })

mysql
  .where('id')
  .gt(6)
  .lt(20)
  .where('name')
  .equals('chandu')
  .limit(3)
  .getRecords((err, res) => {
    if (err) {
      console.log(err.stack)
    } else {
      console.log('------where, where-----', res.rows)
    }
  })

mysql
  .find({ name: 'chandu' })
  .where('id')
  .gte(20)
  .lte(50)
  .getRecords((err, res) => {
    if (err) {
      console.log(err.stack)
    } else {
      console.log('--find, where---', res.rows)
    }
  })

mysql
  .find({ name: 'chandu' })
  .where('id')
  .gte(20)
  .lte(35)
  .sort('id', -1)
  .getRecords((err, res) => {
    if (err) {
      console.log(err.stack)
    } else {
      console.log('---sort by----', res.rows)
    }
  })
