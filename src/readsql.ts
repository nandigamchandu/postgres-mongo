type User = {
  id: number
  name: string
  age: number
}
type myArray = (number | string)[]
type Where = {
  columns?: string[]
  id?: number | number[] | RelationalOperations | LogicOperation
  name?: string | string[] | RelationalOperations | LogicOperation
  age?: number | number[] | RelationalOperations | LogicOperation
  $or?: LogicOperation
  $and?: LogicOperation
}
type ArrayOfValues =
  | number
  | string
  | string[]
  | number[]
  | RelationalOperations
  | undefined
  | Where

type LogicOperation = RelationalOperations | Where

type RelationalOperations = {
  $gt?: number
  $lt?: number
  $gte?: number
  $lte?: number
  $eq?: number | string
  $in?: number[] | string[]
}

const getOperator = (val: string): string => {
  const operators: { [key: string]: string } = {
    $gt: '>',
    $lt: '<',
    $gte: '>=',
    $lte: '<=',
    $or: 'OR',
    $and: 'AND',
    $eq: '=',
    $in: 'IN',
  }
  return operators[val]
}

const isString = (val: unknown): val is string => {
  return typeof val === 'string'
}

const isNumber = (val: unknown): val is number => {
  return typeof val === 'number'
}

const isArray = (val: unknown): val is any[] => {
  return val instanceof Array
}

const addQuotesToString = (lst: (string | number)[]): (string | number)[] => {
  return lst.map(val => (isString(val) ? `"${val}"` : val))
}

const inOperator = (column: string, lst: (string | number)[]): string => {
  const values = addQuotesToString(lst)
  return `${column} IN (${values})`
}

const getKeys = (obj: Where): string[] => {
  return Object.keys(obj)
}

const getValues = (obj: Where): ArrayOfValues[] => {
  return Object.values(obj)
}

const stringEqual = (column: string, value: string): string => {
  return `${column} = "${value}"`
}

const numberEqual = (column: string, value: number): string => {
  return `${column} = ${value}`
}

const andOperation = <T>(noOfProperties: number, obj: T): string => {
  return noOfProperties === 1
    ? `${joinByAnd(checkConditions(obj))}`
    : `(${joinByAnd(checkConditions(obj))})`
}

const orOperation = <T>(noOfProperties: number, obj: T): string => {
  return noOfProperties === 1
    ? `${joinByOr(checkConditions(obj))}`
    : `(${joinByOr(checkConditions(obj))})`
}

const joinByOr = (lst: string[]): string => {
  return lst.join(' OR ')
}

const joinByAnd = (lst: string[]): string => {
  return lst.join(' AND ')
}

const constructConditions = <T>(column: string, obj: T): string[] => {
  const keys = getKeys(obj)
  const values = getValues(obj)
  return keys.map((key, ind) =>
    key === '$or'
      ? `(${joinByOr(constructConditions(column, values[ind]))})`
      : `${column} ${getOperator(key)} ${values[ind]}`,
  )
}

const buildRelations = <T>(key: string, value: T) => {
  if (isString(value)) {
    return stringEqual(key, value)
  } else if (isNumber(value)) {
    return numberEqual(key, value)
  } else if (isArray(value)) {
    return inOperator(key, value)
  } else {
    return joinByAnd(constructConditions(key, value))
  }
}

function checkConditions(obj: Where) {
  const keys = getKeys(obj)
  const values = getValues(obj)
  return values.map((value, ind) => {
    if (keys[ind] === '$or') {
      return orOperation(keys.length, value)
    } else if (keys[ind] === '$and') {
      return andOperation(keys.length, value)
    } else {
      return buildRelations(keys[ind], value)
    }
  })
}

function where(obj: Where) {
  return joinByAnd(checkConditions(obj))
}

function select(columns?: string[]) {
  if (columns) {
    return `SELECT ${columns.join(',')} FROM users`
  } else {
    return `SELECT * FROM users`
  }
}

function read(obj: Where) {
  const { columns, ...object } = obj
  const columnSelect = select(columns)
  const conditions = where(object)
  console.log(`${columnSelect} WHERE ${conditions}`)
}

// SELECT id,name FROM users WHERE id = 5 AND name = "chandu"
read({ id: 5, name: 'chandu', columns: ['id', 'name'] })

// SELECT * FROM users WHERE id > 20 AND id < 60 AND name = "chandu" AND age = 26
read({ id: { $gt: 20, $lt: 60 }, name: 'chandu', age: 26 })

// SELECT * FROM users WHERE (id > 20 OR id < 60) AND name = "chandu" AND age = 26
read({ id: { $or: { $gt: 20, $lt: 60 } }, name: 'chandu', age: 26 })

// SELECT * FROM users WHERE id > 20 AND id < 60 AND name IN ("chandu","chandu2") AND age = 26
read({ id: { $gt: 20, $lt: 60 }, name: ['chandu', 'chandu2'], age: 26 })

// SELECT * FROM users WHERE id IN (20,60) AND name IN ("chandu","chandu2") AND age = 26
read({ id: [20, 60], name: ['chandu', 'chandu2'], age: 26 })

// SELECT id,name FROM users WHERE id IN (20,60) OR name IN ("chandu","chandu2") OR age = 26
read({
  $or: {
    id: [20, 60],
    name: ['chandu', 'chandu2'],
    age: 26,
  },
  columns: ['id', 'name'],
})

// SELECT id,name FROM users WHERE (id IN (20,60) OR name IN ("chandu","chandu2") OR age = 26) AND id = 50
read({
  $or: {
    id: [20, 60],
    name: ['chandu', 'chandu2'],
    age: 26,
  },
  id: 50,
  columns: ['id', 'name'],
})

// SELECT id,name FROM users WHERE (id IN (20,60) AND name IN ("chandu","chandu2")) OR age = 26
read({
  $or: {
    $and: { id: [20, 60], name: ['chandu', 'chandu2'] },
    age: 26,
  },
  columns: ['id', 'name'],
})

// SELECT id,name FROM users WHERE (id IN (20,60) OR name IN ("chandu","chandu2")) AND age = 26
read({
  $and: {
    $or: { id: [20, 60], name: ['chandu', 'chandu2'] },
    age: 26,
  },
  columns: ['id', 'name'],
})
